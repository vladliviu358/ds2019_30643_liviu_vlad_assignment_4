﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace MedicalClinicSDM.Models.ViewModels
{
    public class PillDispenserViewModel
    {
        public int MedicationID { get; set; }
        public int PacientID { get; set; }
        public String PacientName { get; set; }
        public String MedicationName { get; set; }
        public double Dosage { get; set; }
        public DateTime TakenFrom { get; set; }
        public DateTime TakenTo { get; set; }
        public Boolean Taken { get; set; }
        public Boolean FlagNotTaken { get; set; }
    }
}
