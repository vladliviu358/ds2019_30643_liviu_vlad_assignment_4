﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MedicalClinicSDM.Models
{
    public class Notification
    {
        public int ID { get; set; }
        public String StartDate { get; set; }
        public String EndDate { get; set; }
        public String Activity { get; set; }
        public String Recomandation { get; set; }
        public int PacientID { get; set; }
        public Pacient Pacient { get; set; }
    }
}
