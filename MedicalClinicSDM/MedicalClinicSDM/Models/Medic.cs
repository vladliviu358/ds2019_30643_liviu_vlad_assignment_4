﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace MedicalClinicSDM.Models
{
    public class Medic
    {
        [Key]
        public int ID { get; set; }

        [Required]
        public String Username { get; set; }

        [Required]
        [DataType(DataType.Password)]
        public String Password { get; set; }

        public String Name { get; set; }

        [DataType(DataType.Date)]
        public DateTime BirthDate { get; set; }

        public String Gender { get; set; }

        public String Address { get; set; }

        public int AccountType { get; set; }

        public ICollection<Pacient> Pacients { get; set; }
    }
}
