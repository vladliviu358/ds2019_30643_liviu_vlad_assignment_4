﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MedicalClinicSDM.Models
{
    public class CaregiverPacient
    {
        public int CaregiverID { get; set; }
        public Caregiver Caregiver { get; set; }

        public int PacientID { get; set; }
        public Pacient Pacient { get; set; }

    }
}
