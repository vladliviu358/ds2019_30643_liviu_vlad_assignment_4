﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace MedicalClinicSDM.Migrations
{
    public partial class v005 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<DateTime>(
                name: "TakeFrom",
                table: "Medication",
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified));

            migrationBuilder.AddColumn<DateTime>(
                name: "TakeTo",
                table: "Medication",
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified));

            migrationBuilder.AddColumn<bool>(
                name: "Taken",
                table: "Medication",
                nullable: false,
                defaultValue: false);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "TakeFrom",
                table: "Medication");

            migrationBuilder.DropColumn(
                name: "TakeTo",
                table: "Medication");

            migrationBuilder.DropColumn(
                name: "Taken",
                table: "Medication");
        }
    }
}
