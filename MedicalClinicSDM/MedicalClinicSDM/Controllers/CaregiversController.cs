﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using MedicalClinicSDM.Data;
using MedicalClinicSDM.Models;
using Microsoft.AspNetCore.Http;
using System.Text.RegularExpressions;
using Effort.Internal.DbManagement.Schema;

namespace MedicalClinicSDM.Controllers
{
    public class CaregiversController : Controller
    {
        private readonly MedicalClinicSDMContext _context;
        private NotificationsController _notification;

        public CaregiversController(MedicalClinicSDMContext context, NotificationsController notification)
        {
            _context = context;
            _notification = notification;
        }

        // GET: Caregivers
        public async Task<IActionResult> Index()
        {
            ViewBag.TipCont = HttpContext.Session.GetString("TipCont");
            ViewBag.User = HttpContext.Session.GetString("User");
            return View(await _context.Caregiver.ToListAsync());
        }
        public IActionResult AccessDet()
        {
            return RedirectToAction("Details", "Caregivers", new { id = HttpContext.Session.GetInt32("UserID") });
        }


        // GET: Caregivers/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            ViewBag.TipCont = HttpContext.Session.GetString("TipCont");
            ViewBag.User = HttpContext.Session.GetString("User");
            await UpdateNotificationsAsync();
            if (id == null)
            {
                return NotFound();
            }

            var caregiver = await _context.Caregiver
                .FirstOrDefaultAsync(m => m.ID == id);
            if (caregiver == null)
            {
                return NotFound();
            }

            return View(caregiver);
        }

        // GET: Caregivers/Create
        public IActionResult Create()
        {
            ViewBag.TipCont = HttpContext.Session.GetString("TipCont");
            ViewBag.User = HttpContext.Session.GetString("User");
            return View();
        }

        // POST: Caregivers/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("ID,Username,Password,Name,BirthDate,Gender,Address,AccountType")] Caregiver caregiver)
        {
            ViewBag.TipCont = HttpContext.Session.GetString("TipCont");
            ViewBag.User = HttpContext.Session.GetString("User");
            caregiver.AccountType = 3;
            if (ModelState.IsValid)
            {
                _context.Add(caregiver);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            return View(caregiver);
        }

        // GET: Caregivers/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            ViewBag.TipCont = HttpContext.Session.GetString("TipCont");
            ViewBag.User = HttpContext.Session.GetString("User");
            if (id == null)
            {
                return NotFound();
            }

            var caregiver = await _context.Caregiver.FindAsync(id);
            if (caregiver == null)
            {
                return NotFound();
            }
            return View(caregiver);
        }

        // POST: Caregivers/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("ID,Username,Password,Name,BirthDate,Gender,Address")] Caregiver caregiver)
        {
            ViewBag.TipCont = HttpContext.Session.GetString("TipCont");
            ViewBag.User = HttpContext.Session.GetString("User");
            if (id != caregiver.ID)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(caregiver);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!CaregiverExists(caregiver.ID))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            return View(caregiver);
        }

        // GET: Caregivers/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            ViewBag.TipCont = HttpContext.Session.GetString("TipCont");
            ViewBag.User = HttpContext.Session.GetString("User");
            if (id == null)
            {
                return NotFound();
            }

            var caregiver = await _context.Caregiver
                .FirstOrDefaultAsync(m => m.ID == id);
            if (caregiver == null)
            {
                return NotFound();
            }

            return View(caregiver);
        }

        // POST: Caregivers/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            ViewBag.TipCont = HttpContext.Session.GetString("TipCont");
            ViewBag.User = HttpContext.Session.GetString("User");
            var caregiver = await _context.Caregiver.FindAsync(id);
            _context.Caregiver.Remove(caregiver);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool CaregiverExists(int id)
        {
            return _context.Caregiver.Any(e => e.ID == id);
        }

        public async Task UpdateNotificationsAsync()
        {
            string path = "C:/Users/vladl/source/repos/MedicalClinicSDM/MedicalClinicSDM/activity.txt";

            List<Notification> notifications = new List<Notification>();
            string[] data = System.IO.File.ReadAllLines(path);

            await CheckNotificationDBNull();

            await DeleteAllNotifications();

            foreach (string s in data)
            {
                notifications.Add(new Notification { PacientID = 5, StartDate = s.Split("\t\t")[0], EndDate = s.Split("\t\t")[1], Activity = Regex.Replace(s.Split("\t\t")[2], @"\t|\n|\r", "") });
            }

            foreach (Notification tt in notifications)
            {
                if (tt.Activity.Equals("Sleeping"))
                {
                    DateTime sdate = DateTime.Parse(tt.StartDate);
                    DateTime edate = DateTime.Parse(tt.EndDate);
                    if ((edate - sdate).TotalHours >= 9.5)
                    {
                        await _notification.Create(tt);
                        Console.WriteLine("Problem with sleep");
                        
                    }
                }
                if (tt.Activity.Equals("Leaving"))
                {
                    DateTime sdate = DateTime.Parse(tt.StartDate);
                    DateTime edate = DateTime.Parse(tt.EndDate);
                    if ((edate - sdate).TotalHours >= 4)
                    {
                        await _notification.Create(tt);
                        Console.WriteLine("Problem with leaving");
                    }
                }
                if (tt.Activity.Equals("Toileting"))
                {
                    DateTime sdate = DateTime.Parse(tt.StartDate);
                    DateTime edate = DateTime.Parse(tt.EndDate);
                    if ((edate - sdate).TotalHours >= 0.5)
                    {
                        await _notification.Create(tt);
                        Console.WriteLine("Problem with toilet");
                    }
                }
            }
        }

        public async Task DeleteAllNotifications()
        {
            var notif = _context.Notification.ToList();

            foreach(Notification n in notif)
            {
                 _context.Notification.Remove(n);
            }
            await _context.SaveChangesAsync();
        }

        public async Task CheckNotificationDBNull()
        {
            var notif = _context.Notification.ToList();
            if (notif.Any())
            {
                ViewData["CN"] = "yes";
            }
            else
            {
                ViewData["CN"] = "no";
            }
        }
    }
}
