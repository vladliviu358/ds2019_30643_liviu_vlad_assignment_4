#pragma checksum "C:\Users\vladl\source\repos\MedicalClinicSDM\MedicalClinicSDM\Views\_ViewStart.cshtml" "{ff1816ec-aa5e-4d10-87f7-6f4963833460}" "7fa37530e4914ec8799cbffa436f051530a068ae"
// <auto-generated/>
#pragma warning disable 1591
[assembly: global::Microsoft.AspNetCore.Razor.Hosting.RazorCompiledItemAttribute(typeof(AspNetCore.Views__ViewStart), @"mvc.1.0.view", @"/Views/_ViewStart.cshtml")]
namespace AspNetCore
{
    #line hidden
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using Microsoft.AspNetCore.Mvc;
    using Microsoft.AspNetCore.Mvc.Rendering;
    using Microsoft.AspNetCore.Mvc.ViewFeatures;
#nullable restore
#line 1 "C:\Users\vladl\source\repos\MedicalClinicSDM\MedicalClinicSDM\Views\_ViewImports.cshtml"
using MedicalClinicSDM;

#line default
#line hidden
#nullable disable
#nullable restore
#line 2 "C:\Users\vladl\source\repos\MedicalClinicSDM\MedicalClinicSDM\Views\_ViewImports.cshtml"
using MedicalClinicSDM.Models;

#line default
#line hidden
#nullable disable
#nullable restore
#line 1 "C:\Users\vladl\source\repos\MedicalClinicSDM\MedicalClinicSDM\Views\_ViewStart.cshtml"
using Microsoft.AspNetCore.Http;

#line default
#line hidden
#nullable disable
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"7fa37530e4914ec8799cbffa436f051530a068ae", @"/Views/_ViewStart.cshtml")]
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"ad5c05904fffa9817f205109b4ef3af0737c36eb", @"/Views/_ViewImports.cshtml")]
    public class Views__ViewStart : global::Microsoft.AspNetCore.Mvc.Razor.RazorPage<dynamic>
    {
        #pragma warning disable 1998
        public async override global::System.Threading.Tasks.Task ExecuteAsync()
        {
            WriteLiteral("\r\n");
#nullable restore
#line 3 "C:\Users\vladl\source\repos\MedicalClinicSDM\MedicalClinicSDM\Views\_ViewStart.cshtml"
  
    String cont = ViewBag.TipCont;

    if (cont.Equals("1"))
    {
        Layout = "~/Views/Shared/PacientLayout.cshtml";

    }
    else if (cont.Equals("2"))
    {
        Layout = "~/Views/Shared/MedicLayout.cshtml";

    }
    else if (cont.Equals("3"))
    {
        Layout = "~/Views/Shared/CaregiverLayout.cshtml";
    }
    else
    {
        Layout = "~/Views/Shared/_Layout.cshtml";
    }

#line default
#line hidden
#nullable disable
        }
        #pragma warning restore 1998
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.ViewFeatures.IModelExpressionProvider ModelExpressionProvider { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IUrlHelper Url { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IViewComponentHelper Component { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IJsonHelper Json { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IHtmlHelper<dynamic> Html { get; private set; }
    }
}
#pragma warning restore 1591
